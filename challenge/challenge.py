import csv
import re
import pickle
import pandas as pd
import numpy as np
import math
import flask
import nltk
import sklearn
from math import sqrt
from flask import Flask, request
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk import pos_tag
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.metrics import mean_squared_error
from .preprocess import *
nltk.download('averaged_perceptron_tagger')

app = Flask(__name__)

@app.route('/genres/train', methods = ['POST'])
def parse_train():
    n_components = 0
    algo = 'qda'
    
    # Prepare the input training data, dataset and labels :
    dt = request.data.decode('utf-8')
    csv_ar2D = [[v for k, v in row.items()] for row in csv.DictReader(dt.splitlines(), skipinitialspace=True)]
    df = pd.DataFrame(csv_ar2D, columns=['movie_id','synopsis','genres'])
    
    genres = df['genres'].to_numpy()
    
    # Prepare labels :  
    tf_vectorizer_components = CountVectorizer(binary=True, token_pattern='[\w\-]+')
    tf_components = tf_vectorizer_components.fit_transform(genres)
    ar_components = tf_components.toarray()
    components = tf_vectorizer_components.vocabulary_
    n_components = len(components)
    
    # Check if the classes are unballanced and ballance if necessary:
    nb_docs_per_class = {cl:np.sum(ar_components[:,cl]) for cl in range(ar_components.shape[1])}
    
    max_val_class = max(nb_docs_per_class.values())
    nb_classes_to_parse = dict(nb_docs_per_class)
    maxlabel = list(nb_classes_to_parse.keys())[list(nb_classes_to_parse.values()).index(max_val_class)]
    
    ycols = ['y'+str(i) for i in range(len(nb_docs_per_class))]
    
    labels = pd.DataFrame(ar_components, columns=ycols)
    newdf = pd.merge(df, labels, left_index=True, right_index=True, how='inner')
    
    del df
    
    limit=7000
    print("Start pre process")
    (X,Y,tf_vectorizer, tf_transformer) = preprocess_get_xy_for_linear(newdf, limit, nb_docs_per_class, ycols, algo)
    
    del newdf
    
    print("PRE PROCESS OK")
    Y = Y.astype('int')
    Y = Y.ravel()
    
    # Run classification algorithm  
    model = QuadraticDiscriminantAnalysis(tol=1.0e-3)
    model.fit(X.toarray(),Y)
    
    # Save the model as a pickle in a file
    save_obj(model, 'model_'+algo) 
    save_obj(tf_vectorizer, 'tf_vectorizer_'+algo)
    save_obj(tf_transformer, 'tf_transformer_'+algo)
    save_obj(components, 'genres_indexes_'+algo) 
    
    return "End"
    
@app.route('/genres/predict', methods = ['POST'])
def parse_prediction():
    algo = 'qda'
    
    dt = request.data.decode('utf-8')
    csv_ar2D = [[v for k, v in row.items()]
        for row in csv.DictReader(dt.splitlines(), skipinitialspace=True)]
    df = pd.DataFrame(csv_ar2D, columns=['movie_id','synopsis'])
    
    model = load_obj('model_'+algo) 
    tf_vectorizer = load_obj('tf_vectorizer_'+algo)
    tf_transformer = load_obj('tf_transformer_'+algo)
    components = load_obj('genres_indexes_'+algo)
    cl = model.classes_
    voc = tf_vectorizer.vocabulary_
    
    mymeans = model.means_.T # rows:features (words) columns:classes 
    
    descriptions = df['synopsis'].apply(lambda x : preprocess_description(x)).to_numpy()
    
    X = tf_vectorizer.transform(descriptions)
    X = tf_transformer.transform(X)
    l = []
    
    def funclog(x):
        if x<=0.0:
            return -10
        else: 
            return math.log(x)
    vflog = np.vectorize(funclog)
    
    for myindex in range(X.shape[0]):
        X0 = X.getrow(myindex).toarray()
        
        count=0
        wt = np.zeros((1,mymeans.shape[1]))
        for wi in range(X0.shape[1]):
            if X0[0][wi] > 0:
                count = count + 1
                wa = vflog(mymeans[wi,:])
                wtt = np.add(wt, wa)
                wt = wtt 
                
        currpred = "" 
        if count>0:
            wt = wt / float(count)
            
            list_classes_indices = parse_first_five_qda(model,cl,wt[0])
            list5 = list_classes_indices[0:5]
            list5 = [int(i) for i in list5]
            gen = [g.capitalize() for g,i in components.items() if i in list5]
            currpred = " ".join(gen)
        
        l.insert(myindex, currpred)
        if myindex%1000==0:
            print("Parsed : "+str(myindex)+" / "+str(X.shape[0]))    
   
    df = df.assign(predicted_genres=pd.Series(l).values)
    df = df.drop(columns=['synopsis'])
    r = df.to_csv(index=False)
    return r
    
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)




