import re
import pandas as pd
import numpy as np
import pickle
import sklearn
import nltk
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.decomposition import PCA
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk import pos_tag, sent_tokenize
 
'''
Description : Ballances input training data using only samples with one class
'''
def ballance_data_samples_classes_11(df, limit, nb_docs_per_class, ycols):
    nbclasses = len(nb_docs_per_class)
    newdf = pd.DataFrame(columns=df.columns)
    
    mymax = 0
    nb_docs_per_class_unique = dict()
    for class_index, v in nb_docs_per_class.items():
        cn = 'y'+str(class_index)
        zerocols = list(ycols)
        zerocols.remove(cn)
        df_try_zeros = df[df[zerocols].eq(0).all(1)]
        df_try = df_try_zeros[df_try_zeros[cn].eq(1)]
        nb_docs_per_class_unique[class_index] = df_try.shape[0]
        if (df_try.shape[0]>mymax):
            mymax = df_try.shape[0]
   
    for class_index, v in nb_docs_per_class_unique.items():
        cn = 'y'+str(class_index)
        zerocols = list(ycols)
        
        zerocols.remove(cn)
        df_try_zeros = df[df[zerocols].eq(0).all(1)]
        df_try = df_try_zeros[df_try_zeros[cn].eq(1)]
        indexes = df_try.index
        
        if v<limit:
            df_mult = int(round(limit/v))
            if df_mult > 0:
                newdf = newdf.append([df_try] * df_mult, ignore_index=True)
            elif df_mult==0:
                to_append = df_try.apply(lambda x: x.sample(limit-v).reset_index(drop=True))
                newdf = newdf.append(df_try)
                newdf = newdf.append(to_append)
        elif v>limit:
            sample = df_try.sample(limit).reset_index(drop=True)
            newdf = newdf.append(sample)
        else:
            newdf = newdf.append(df_try)
       
    return newdf

def parse_first_five(model,cl,X0):
    pp = model.predict_proba(X0)
    index_pred = pp[0].argsort()[-5:][::-1]
    pred_classes = [cl[i] for i in index_pred]
    return pred_classes

def parse_first_five_qda(model,cl,pp):
    index_pred = pp.argsort()[-5:][::-1]
    pred_classes = [cl[i] for i in index_pred]
    pred_classes = [int(i) for i in pred_classes]
    return pred_classes

'''
Description: Saves an object (model, other) into a pickle file
'''
def save_obj(obj, filename):
    with open(filename + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

'''
Description: Loads an object (model, other) from a pickle file
'''
def load_obj(filename):
    with open(filename + '.pkl', 'rb') as f:
        return pickle.load(f)

'''
Description: Preprocesses data for training, linear model
'''
def preprocess_get_xy_for_linear(df, limit, nb_docs_per_class, ycols, algo):
    descriptions = df['synopsis'].apply(lambda x : preprocess_description(x))
    df['synopsis'] = descriptions
    
    df_input = ballance_data_samples_classes_11(df, limit, nb_docs_per_class, ycols)
    df_labels_ballanced = df_input.drop(columns=['movie_id','synopsis', 'genres'])
    np_labels_ballanced = df_labels_ballanced.to_numpy()
    nb_docs_per_class_ballanced = {cl:np.sum(np_labels_ballanced[:,cl])
        for cl in range(np_labels_ballanced.shape[1])}
    
    # Vectorize dataset with frequences:
    descriptions = df_input['synopsis'].to_numpy()
    
    tf_vectorizer = CountVectorizer(max_df=0.5, min_df=2, max_features=2000)
    # best lda:
    if algo=='lda':
        tf_vectorizer = CountVectorizer(max_df=0.5, min_df=2, max_features=2000)
    # best gnb:
    elif algo=='gnb':
        tf_vectorizer = CountVectorizer(max_df=0.9, max_features=20000)
    elif algo == 'mlp':
        tf_vectorizer = CountVectorizer(max_df=0.95, max_features=20000)
    # Random forest:
    elif algo == 'rfc':
        tf_vectorizer = CountVectorizer(max_df=0.5, max_features=10000)
    # best quadratic discriminant analysis
    elif algo == 'qda':
         tf_vectorizer = CountVectorizer(max_df=0.5, max_features=6000)
    
    tf_vectorizer.fit(descriptions)
    X = tf_vectorizer.transform(descriptions)
    nb_features = X.shape[1]
    
    tf_transformer = TfidfTransformer().fit(X)
    X = tf_transformer.transform(X)
    
    # Prepare difference : samples with several classes
    df_diff = pd.concat([df, df_input, df_input]).drop_duplicates(keep=False)
    df_labels_diff = df_diff.drop(columns=['movie_id','synopsis', 'genres'])
    	
    def getClassIndex(row):
        xx = [col for col in df_labels_ballanced.columns if row[col]==1][0]
        xi = int(xx[1:])
        return xi
        
    Y = df_labels_ballanced.apply(getClassIndex, axis=1).to_numpy()
    
    return (X,Y,tf_vectorizer, tf_transformer)

'''
Preprocesses description. Tokenizes, searches for meaningful words, stems them. No use to split into sentences before.
'''

def preprocess_description(line):
    stemmer = SnowballStemmer('english')
    stop_words = stopwords.words('english')
    tokens_pos_all = nltk.pos_tag(word_tokenize(line))	
    #meaningful_pos_tokens = [x[0] for x in tokens_pos_all if re.match('(JJ)', x[1]) or re.search('^(NN|NNS)$', x[1])]
    #meaningful_pos_tokens = [x[0] for x in tokens_pos_all if re.match('(JJ|VB|RB)', x[1]) or re.search('^(NN|NNS)$', x[1])]
    meaningful_pos_tokens = [x[0] for x in tokens_pos_all if re.match('(JJ|NN|VB|RB)', x[1])]
    #meaningful_pos_tokens = [x[0] for x in tokens_pos_all if re.search('^(NN|NNS)$', x[1])]
    meaningful_tokens = [x.lower() for x in meaningful_pos_tokens if not x in stop_words and x.isalpha() and len(x)>2]
    meaningful_stems = [stemmer.stem(x) for x in meaningful_tokens]
    result = " ".join(meaningful_stems)
    return result
 

